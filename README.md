# site-typo3

This is a generic Dockerfile for Typo3 9 instances.

It includes php 7.2, graphcsmagic and ssl modules.

Volumes are configured for
- fileadmin
- uploads
- typo3temp
- var
- config
- ssl

