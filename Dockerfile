FROM php:7.2-apache

ENV APACHE_DOCUMENT_ROOT /var/www/html/public/
ENV COMPOSER_ALLOW_SUPERUSER 1

WORKDIR /var/www/html/
COPY . /var/www/html/

RUN apt-get update -yqq && \
    apt-get dist-upgrade -yqq && \
    echo "Europe/Berlin" > /etc/timezone && \
    rm /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get install -yqq \
        libcurl4-gnutls-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libicu-dev \
        libpng-dev \
        libbz2-dev \
        graphicsmagick \
        apt-transport-https \
        git \
        libxml2-dev \
        wget \
        unzip \
        zlib1g-dev && \
    # Install PHP extensions
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install -j$(nproc) \
        intl \
        gd \
        zip \
        bz2 \
        opcache \
        mysqli && \
    echo "memory_limit=1024M\nalways_populate_raw_post_data = -1\nmax_execution_time = 240\nmax_input_vars = 1500\nupload_max_filesize = 32M\npost_max_size = 32M" > /usr/local/etc/php/conf.d/memory-limit.ini && \
    echo "date.timezone = Europe/Berlin" > /usr/local/etc/php/conf.d/timezone.ini && \
    pecl install \
        apcu && \
    docker-php-ext-enable \
        apcu && \
    docker-php-source delete && \
    rm -rf /var/lib/apt/lists/* && \
    # Install and run Composer
    curl -sS https://getcomposer.org/installer | php && \
    php composer.phar install --prefer-dist --no-progress --no-dev --no-suggest --optimize-autoloader --classmap-authoritative  --no-interaction --ignore-platform-reqs && \
    php composer.phar clear-cache && \
    chown -R www-data:www-data /var/www/html && \
    chown -R /etc/ssl && \
    chmod -R 777 ${APACHE_DOCUMENT_ROOT}typo3temp

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf && \
    sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf && \
    sed -ri -e 's!ssl-cert-snakeoil.pem!${SERVER_NAME}.pem!g' /etc/apache2/apache2.conf /etc/apache2/sites-available/*conf && \
    sed -ri -e 's!ssl-cert-snakeoil.key!${SERVER_NAME}.key!g' /etc/apache2/apache2.conf /etc/apache2/sites-available/*conf && \
    a2enmod ssl headers rewrite && \
    a2ensite default-ssl

VOLUME ${APACHE_DOCUMENT_ROOT}fileadmin
VOLUME ${APACHE_DOCUMENT_ROOT}uploads
VOLUME ${APACHE_DOCUMENT_ROOT}typo3temp
VOLUME ${WORKDIR}var
VOLUME ${WORKDIR}config
VOLUME /etc/ssl
